from .trendAnalysis import TrendAnalysis, adf_test
from .smoothingFilters import AggregateData, MovingAverage, SavGol_smoothing
from .plottingTools import plot_time_series
from .ets import ETS_decomposition
