from bokeh.plotting import figure
from bokeh.io import output_file, output_notebook, show
from bokeh.models import ColumnDataSource
from bokeh.models import HoverTool
from bokeh.models import DatetimeTickFormatter



def plot_time_series(data, fig , fig_size = [800, 350], title = "", legend = "",
                     labels = ["Date", ""], linewidth = 1, color = "blue", linestyle = "solid", alpha = 1):
    
    fig.plot_width = fig_size[0]
    fig.plot_height = fig_size[1]
    
    source = ColumnDataSource()
    source.add(data.index, name="date")
    source.add(data.values, name="values")
    source.add(data.index.strftime("%Y-%m-%d %H:%M:%S"), "date_formatted")
        
    #create a line plot
    fig.line(source = source, x="date", y="values",
             legend = legend, line_color = color, line_dash = linestyle, line_alpha = alpha, line_width = linewidth)
    
    fig.title.text= title
    fig.title.text_font = 'Helvetica'
    fig.title.text_font_size = '18px'
    fig.title.align = 'center'

    fig.legend.background_fill_alpha = 0.6
    fig.legend.label_text_font = 'Helvetica'

    fig.xaxis.axis_label = labels[0]
    fig.yaxis.axis_label = labels[1]

    hover = HoverTool(tooltips = [("Value: ", "@values"), ("Timestamp: ", "@date_formatted")])
    hover.formatters = {'Timestamp': "datetime"}
    fig.add_tools(hover)
    fig.toolbar_location = "above" 

    return fig 