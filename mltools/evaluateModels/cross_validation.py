from sklearn.model_selection import cross_validate, GridSearchCV, RandomizedSearchCV, KFold
from sklearn.metrics import make_scorer
from scipy.stats import *
import numpy as np
import pandas as pd
import json
import scipy
from scipy.stats import t

# Classification metrics
from sklearn.metrics import accuracy_score, auc, confusion_matrix, f1_score
from sklearn.metrics import matthews_corrcoef, precision_score, recall_score

# Regression metrics
from sklearn.metrics import mean_absolute_error, mean_squared_error, median_absolute_error, residual_mean_squared_error
from sklearn.metrics import mean_squared_log_error, r2_score


class CrossValidation():

    """ This class compute the k-fold cross validation for a list of models (and their specific parameters)
        in input.
        The models that we want to apply are written into a json file. That file is read by the class into
        the init function. Also, in this configuration file the users can specify the list of parameters that
        they want to test.
        In this case the class compute a grid search cv for identify the best model by the optimization of a
        specific loss function. If no parameters are specify, or no params_file is pass to the class, all models
        will be instantiated with the default configuration.

        At the end of the fitting procedure, the class return a list of models (the best result of grid search)
        and a pandas DataFrame which contains the mean and the standard deviation of all score metrics that the
        user want to analyze. It's also possible to obtain the single folds results.
    Args:
    -----
        models (list)         >>> The list of models that we want to compute. See the results of get_models_info
                                  for the list of models that can be used.

        scores (list)         >>> The list of scores that we want to compute. See the results of get_scores_info
                                  for the list of scores that can be used.

        k (int)               >>> Optional. Number of folds. Default is equal to 10.

        params_file  (string) >>> Optional. File which contains the list of parameters to test for each model.
    """

    def __init__(self, models, scores, params_file=None):

        if type(models) == list:
            self.models = models
        else:
            self.models = [models]
        self.scores = scores
        config_file = 'model_config.json'
        scores_file = 'scores_file.json'
        config_path = 'mltools/evaluateModels/config_files/'
        with open(config_path + config_file) as config:
            self.conf = json.loads(config.read())
        if params_file:
            with open(params_file) as param:
                self.params_file = json.loads(param.read())

                # check if the models specified in input are contained into the param file
                for model in self.models:
                    if model not in self.params_file["MODELS_LIST"].keys():
                        raise Exception("The method {} it's not defined into the param file section MODELS_LIST.".format(model))
                    elif model not in self.params_file["TRAINING_STRATEGY"].keys():
                        raise Exception("The method {} it's not defined into the param file section TRAINING_STRATEGY.".format(model))
        else:
            self.params_file = params_file

        with open(config_path + scores_file) as score:
            self.scores_dict = json.loads(score.read())

    def get_models_info(self):
        for key in self.conf.keys():
            print(key)

    def get_scores_info(self):
        for key in self.scores_dict.keys():
            print(key)

    def set_models(self, models):
        if type(models) == list:
            self.models = models
        else:
            self.models = [models]

    def create_model(self, model):
        """ This function create an instance of the specified model, by running che python code in the conf file.

            Args:
            -----
                model (string) >>> name of the model that we want to create.

            Returns:
            --------
                an instance of the specified model.
        """

        exec(self.conf[model]['path'])
        return eval(self.conf[model]['init'])

    def set_model_params(self, model, param_dict):
        """ This function set the dict of the parameters for a specific ML models given in input.

            Args:
            -----
                model (instance of a ML model) >>> the instance of a model that we want to setting.
                param_dict (dict) >>> a dict that contains as a keys the name of the parameters and the items as
                the values that we want to set.

            Returns:
            --------
                an instance of the specified model.
        """

        try:
            model.set_params(**param_dict)
        except Exception:
            Exception("Impossible to set the parameters in {} model.".format(model))
        return model

    def fit_cv(self, train_data, target_variable, folds_results=False, k=10, seed=123):
        """ This function fit the cross validation for the models read by the class during the initialization.

            Args:
            -----
                train_data (numpy array)        >>> an array that contains the features data.
                target_variable (numpy array)   >>> an array which contains the target variable to predict.
                loss_function (sklearn.metrics) >>> Optional. The score that you want to use for compute the best
                                                    model in grid search cv.
                folds_results (boolean)         >>> Optional. Set to true if you want the results of your models
                                                    for each fold. Default is set to False.

            Returns:
            --------
                A tuple of a pandas Dataframe that contain the scoring results and a dict that has as a keys the
                name of models used and as values the instances built.
        """
        results_dict = {}
        models_dict = {}
        kf = KFold(k, shuffle=True, random_state=seed).get_n_splits(train_data)
        for model in self.models:
            clf = self.create_model(model)
            if self.params_file:
                #extract the dictionary containing the params of cv methods
                cv_params = self.params_file["TRAINING_STRATEGY"][model]

                if cv_params["search_method"] == "cv":  
                        model_to_fit = self.set_model_params(clf, self.params_file["MODELS_LIST"][model])
                        print("Create a model: {}".format(model_to_fit))
                        print("Evaluate the model with cross validation...")
                else:

                    if cv_params["search_method"] == "grid_search":
                            gs = GridSearchCV(clf, param_grid=self.params_file["MODELS_LIST"][model], cv=kf,
                                               **cv_params['search_params'])
                            print("Searching the best {} with grid search cv...".format(model))

                    elif cv_params["search_method"] == "random_search":
                            distribution = self._createDistributions(self.params_file["MODELS_LIST"][model])
                            gs = RandomizedSearchCV(clf, param_distributions=distribution, cv=kf, 
                                                     **cv_params['search_params'])
                            print("Searching the best {} with random search cv...".format(model))

                    elif cv_params["search_method"] == "bayesian_search":        
                            gs = BayesianCV() #NOT IMPLEMENTED

                    else:
                        raise Exception("Error: invalid search_method. Use 'cv', 'grid_search', 'random_search' or 'bayesian_search'.")

                    results = gs.fit(train_data, target_variable)
                    model_to_fit = results.best_estimator_
                    print("Best_estimator: {}\nBest_scores: {}".format(model_to_fit, results.best_score_))
                    print("Evaluate the best model configuration with a new cross validation...\n")

            elif not self.params_file:  # if no parameters are specified we use the default configuration
                model_to_fit = clf
                print("Create a model: {}".format(model_to_fit))
                print("Evaluate the model with cross validation...")

            results = cross_validate(model_to_fit, train_data, target_variable,
                                      scoring=self._select_score(), cv=kf)

            results_dict[model] = results
            models_dict[model] = model_to_fit

        print("Finish")
        return self._create_DataFrame(results_dict, folds_results), models_dict

    def _createDistributions(self, model_params):
        distribution = {}
        for key, value in model_params.items():
            if type(value) == str:
                distribution[key] = eval(value)
            else:
                distribution[key] = value
        return distribution

    def _select_score(self):
        """ This function compute the score by extract it from the configuration file.

            Returns:
            --------
                an instance of the sklearn metric.
        """

        custom_score = {k: eval(self.scores_dict[k]) for k in self.scores}
        return custom_score

    def _mean_confidence_interval(self, data, alpha):

        k = len(data)
        m, std_error = np.mean(data), scipy.stats.sem(data)
        t_quantile = t.ppf(1-alpha/2, k-1)
        radius =  t_quantile * std_error
        lower_bound = m - radius
        upper_bound = m + radius
        
        ci = [np.round(lower_bound, 4), np.round(upper_bound, 4)]
        ci = list(np.clip(ci, a_min = 0, a_max = 1))
        return ci 

    def _create_DataFrame(self, results, folds_results):
        """ This function create a pandas DataFrame that contains the scoring results for each models.
            In particular, for each score are reported the mean and the standard deviation. Also, if folds_results
            was setted to True, dataframe contains also a column with the list of the results for each single fold.

            Args:
            -----
                results (dict)          >>> dictionary that contains the results for each model tested.
                folds_results (boolean) >>> if it's True, are reported also the single fold results.

            Returns:
            --------
                pandas.DataFrame.
        """

        df_results = pd.DataFrame.from_dict(results, orient='index')

        output = pd.DataFrame()
        output['computation_total'] = (df_results['fit_time'] + df_results['score_time']).apply(np.sum)

        for col in df_results.columns[2:]:
            col_mean = col + '_mean'
            output[col_mean] = df_results[col].apply(np.mean)
            col_sd = col + '_sd'
            output[col_sd] = df_results[col].apply(np.std)

            col_ci1 = col + '_ci_95%'
            output[col_ci1] = df_results[col].apply(self._mean_confidence_interval, args = (0.05,))
            col_ci2 = col + '_ci_99%'
            output[col_ci2] =  df_results[col].apply(self._mean_confidence_interval, args = (0.01,))


            if folds_results:
                col_partial = col + '_partial'
                output[col_partial] = df_results[col]

        return output.T
