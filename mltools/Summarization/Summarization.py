try: from gensim.summarization.summarizer import summarize as gensim_summarize
except: print("there isn't gensim library") 
    
try: from rouge import Rouge 
except: print("there isn't rogue library")
    
try: import pandas as pd
except: print("there isn't pandas library")
    
    
try: import numpy as np
except: print("there isn't numpy library")    

    
try: from sumeval.metrics.bleu import BLEUCalculator
except: print("there isn't sumeval library")
    
try: from summa.summarizer import summarize as summa_summarize
except: print("there isn't summa library")
    
try: from summa import keywords
except: print("there isn't summa library")
    
    
try: from sumy.parsers.plaintext import PlaintextParser 
except: print("there isn't sumy library")
    
try: from sumy.nlp.tokenizers import Tokenizer 
except: print("there isn't sumy library")
    
try: from sumy.summarizers.lex_rank import LexRankSummarizer
except: print("there isn't sumy library")
    
try: from sumy.summarizers.text_rank import TextRankSummarizer
except: print("there isn't sumy library")
    
try: from sumy.summarizers.lsa import LsaSummarizer
except: print("there isn't sumy library")
    
try: from sumy.summarizers.luhn import LuhnSummarizer
except: print("there isn't sumy library")
    
    
try: from langdetect import detect
except: print("there isn't langdetect library")    


    





    
    
"""

#                     https://nlpforhackers.io/textrank-text-summarization/


Summarization è il processo che permette di ridimensionare il contenuto di un testo. L'idea è di prendere un
sottoinsieme del testo, contenente le principali informazioni.
Esistono due tecniche per fare questo: -Extraction -Abstraction. Il primo permette di ottenere le sentenze
dallo stesso testo, ad esempio, se X insieme contenente tutte le sentenze di un testo, si ottiene un nuovo
insieme Y, contenente solo le piu importanti sentenze di X. Quindi Y è contenuto in X. Invece l'abstraction
procederà a costruire delle nuove sentenze, quindi in questo caso, l'insieme Y avrà sentenze diverse da quelle
presenti nell'insieme X.

Per adesso, non si userà il metodo abstraction.


------------------------------PAGERANK-------------------------------------------

Per spiegare il metodo extraction si utilizza la tecnica utilizzata per il PageRank. L'idea del PageRank è
che se abbiamo un'insieme di pagine web tra loro connesse, per far si che un motore di ricerca mostri
in ordine tra la piu importante alla meno importante, si deve associare a ciascuna un rango.
L'idea è che possiamo definire l'importanza di una pagina in base a quanti riferimenti ci siano nelle
altre pagine. Quindi se ad esempio una pagina ha riferimenti da altre due pagine e invece un'altra
tre riferimenti, sarà quest'ultima ritenuta piu importante.

links = {
    'webpage-1': set(['webpage-2', 'webpage-4', 'webpage-5', 'webpage-6', 'webpage-8', 'webpage-9', 'webpage-10']),
    'webpage-2': set(['webpage-5', 'webpage-6']),
    'webpage-3': set(['webpage-10']),
    'webpage-4': set(['webpage-9']),
    'webpage-5': set(['webpage-2', 'webpage-4']),
    'webpage-6': set([]), # dangling page
    'webpage-7': set(['webpage-1', 'webpage-3', 'webpage-4']),
    'webpage-8': set(['webpage-1']),
    'webpage-9': set(['webpage-1', 'webpage-2', 'webpage-3', 'webpage-8', 'webpage-10']),
    'webpage-10': set(['webpage-2', 'webpage-3', 'webpage-8', 'webpage-9']),
}

Si scrivono come

# {'webpage-10': 3, 'webpage-9': 0, 'webpage-8': 1, 'webpage-1': 2, 'webpage-3': 4, 'webpage-2': 5, 'webpage-5': 6, 'webpage-4': 7, 'webpage-7': 8, 'webpage-6': 9}

SI calcola la matrice di transizione, ossia si determina la probabilità passando da da una pagina all'altra.Ad esempio,
dalla pagina 1, abbiamo la possibilità di passare in 7 pagine, quindi la probabilità di transitare dalla 1 alla pagina 2
sarà 1/7, questo valerà anche per transitare dalla 1 alla 4,5,6,8,9 o 10. Invece la probabilità di passare dalla 1 alla
3 o 7, sarà ovviamente 0. Quindi

web1:
[[0.         0.14285714 0.         0.14285714 0.14285714 0.14285714
  0.         0.14285714 0.14285714 0.14285714]
  
web2:
 [0.         0.         0.         0.         0.5        0.5
  0.         0.         0.         0.        ]
  
web3:
 [0.         0.         0.         0.         0.         0.
  0.         0.         0.         1.        ]
  
web4:  
 [0.         0.         0.         0.         0.         0.
  0.         0.         1.         0.        ]
  
web5:
 [0.         0.5        0.         0.5        0.         0.
  0.         0.         0.         0.        ]
  
web6:
 [0.1        0.1        0.1        0.1        0.1        0.1
  0.1        0.1        0.1        0.1       ]
  
web7:

 [0.33333333 0.         0.33333333 0.33333333 0.         0.
  0.         0.         0.         0.        ]
  
web8:
 [1.         0.         0.         0.         0.         0.
  0.         0.         0.         0.        ]
  
web9:
 [0.2        0.2        0.2        0.         0.         0.
  0.         0.2        0.         0.2       ]
  
web10:
 [0.         0.25       0.25       0.         0.         0.
  0.         0.25       0.25       0.        ]]


Si applica ora una funzione (Brinn and Page 1998) e si ottiene il rango per ogni 

[ 0.13933698,  0.09044235,  0.1300934 ,  0.13148714,  0.08116465, 0.1305122 ,  0.09427366,  0.085402  ,  0.02301397,  0.09427366]


---------------------------------TEXTRANK----------------------------------------------




Ora si passa dal PageRank al TextRank

Quindi tornando all'inizio, se abbiamo un testo, lo dividiamo in sentenze, e ogni sentenza appartiene a un insieme X.
Per determinare l'importanza di ogni sentenza, si dovrà procedere a vedere la similarità tra sentenze, in modo tale
che quella che ha piu "richiami" sarà classificata come la migliore.

Viene utilizzato principalmente il coseno di similitudine, che ovviamente richiede la trasformazione di una sentenza
in valori numerici.


Prendiamo questo esempio:

   a: Julie loves me more than Linda loves me

   b: Jane likes me more than Julie loves me
   
   
Scriviamo la sentenza, evitando le parole che si ripetono:

    me Julie loves Linda than more likes Jane

Si prende questa frase cosi ottenuta, per ogni parola qui presente, si vede quante volte è presente nella
prima sentenza e quante sulla seconda.

        a   b
        
  me    2   2
 Jane   0   1
Julie   1   1
Linda   1   0
likes   0   1
loves   2   1
 more   1   1
 than   1   1
 
 
a: [2, 0, 1, 1, 0, 2, 1, 1]

b: [2, 1, 1, 0, 1, 1, 1, 1]


Cosi possiamo calcolarlo in questo modo:

                    (2*2+0*1+1*1+1*0+2*1+1*1+1*1)
-------------------------------------------------------------------
(2^2+0^2+1^2+1^2+0^2+2^2+1^2+1^2)^0.5  + (2^2+1^2+1^2+0^2+1^2+1^2+1^2+1^2)^0.5            


Molto spesso è consigliato, togliere le stopwords, ad esempio articoli, preposizioni....

Tale valore va da 0 a 1, dove avrà valore 1 se sono identiche e 0 se completamente differenti. DI solito
si utilizza 1 - coseno di similitudine, con significato opposto.

Come prima, se si hanno "n" sentenze, si costruisce una matrice K=nxn, dove ogni K(i,j) indica il 1-coseno di similitudine
tra la sentenza i e la sentenza j.


[[0.         0.02171602 0.02438459 ... 0.01056602 0.02113203 0.0224139 ]
 [0.01642812 0.         0.00853223 ... 0.00646989 0.01940966 0.0137247 ]
 [0.0326456  0.01509956 0.         ... 0.00642841 0.01928522 0.02727342]
 ...
 [0.0154814  0.01253106 0.00703547 ... 0.         0.03200945 0.0150894 ]
 [0.01453939 0.01765287 0.00991107 ... 0.01503088 0.         0.02125687]
 [0.0179617  0.01453869 0.01632527 ... 0.00825283 0.02475849 0.        ]]
 
 
Ora, come nel PageRank, si applica una funzione, ad esempio quella di Brinn and Page 1998, e si ottiene il rango
per ogni sentenza.

[0.01121618 0.01416586 0.00875955 0.01720666 0.01097157 0.00992082
 0.01200115 0.00168689 0.01309704 0.01454477 0.01097302 0.00816622
 0.00878952 0.00681878 0.01487246 0.00980958 0.01631003 0.01077275
 0.01016464 0.00164935 0.01201909 0.01345858 0.01054308 0.01246479
 0.00189483 0.00848166 0.01189495 0.00226414 0.00871032 0.01280831
 0.01201529 0.0091875  0.0132896  0.01413383 0.0083716  0.01075297
 0.01131163 0.00866148 0.00781889 0.00798044 0.01358306 0.00816582
 0.00787996 0.00986716 0.01055781 0.01245516 0.00559471 0.00901723
 0.01349356 0.01029195 0.00615882 0.00646616 0.00680286 0.01227652
 0.00915185 0.01146637 0.01031809 0.00676416 0.00947115 0.00730678
 0.00687184 0.0022958  0.01043746 0.00837449 0.00795457 0.00182517
 0.0082859  0.00822529 0.01303836 0.00667884 0.00853634 0.01013017
 0.00852789 0.01224895 0.00630303 0.01017482 0.01098564 0.01494719
 0.00789689 0.01391384 0.01242932 0.0017009  0.01639154 0.01258241
 0.01145775 0.01336677 0.01761915 0.01135941 0.01403542 0.01388494
 0.01070144 0.01214065 0.01511402 0.01502419 0.01015592 0.00820689
 0.01549813 0.0136297 ]
 
 
La si ordina per grandezza, e si sceglie quante sentenze si vuole prendere. Ad esempio se si sceglie di prendere solo
N=5, vuol dire che si prendono le 5 sentenze piu importanti.

1. `` Only a relative handful of such reports was received '' , the jury said , `` considering the widespread interest in the election , the number of voters and the size of this city '' .
2. Nevertheless , `` we feel that in the future Fulton County should receive some portion of these available funds '' , the jurors said .
3. -- After a long , hot controversy , Miller County has a new school superintendent , elected , as a policeman put it , in the `` coolest election I ever saw in this county '' .
4. `` This was the coolest , calmest election I ever saw '' , Colquitt Policeman Tom Williams said .
5. `` Everything went real smooth '' , the sheriff said .


Cosi si è ottenuto il summary del testo iniziale.



-----------------------EVALUTATION------------------------------------

Se abbiamo un summary, ad esempio scritto a mano, e effettuiamo un Automatic summarization con il textrank, e vogliamo
valutare la performance del modello (esistono varie tecniche, quindi è utile valutarne le performance), si possono 
utilizzare delle metriche.

Quello che si utilizza è il ROUGE.
https://rxnlp.com/how-rouge-works-for-evaluation-of-summarization-tasks/#.W406nnUzY5k

Ad esempio date due sentenze:

summary: the cat was found under the bed

summary_p: the cat was under the bed

dove summary_p è il summary ottenuto con il textrank invece il summary è quello che abbiamo scritto a mano.

Si può calcolare la RECALL

numero delle parole di summary_p presenti in summary / totale parole di summary_p  = 6   /  6  =  1

Ma se abbiamo un summary troppo lunga, potrebbe aver preso tutte le parole il nostro summary_p, quindi 
darebbe risultato 1. Per evitare questo si può calcolare la PRECISIONE

numero delle parole di summary p presenti in summary / totale parole di summary = 6 / 7 


Ma preferibilmente si potrebbe utilzizare F-Measure come media ponderata trai due 


#------------------------ROUGE N, ROUGE L---------------------------------------------

Il caso appenna annunciato è il ROUCE-1 dove uno indica il n-gram della sentenza


summary:

the cat, 
cat was, 
was found, 
found under, 
under the, 
the bed

summary_p:
 
the cat, 
cat was, 
was under, 
under the, 
the bed
 
In questo caso la RECALL è 4/5 invece la precision 4/6


ROUGE-L utilizza invece dei n-gram modelli LCS,Longest Common Subsequence, e quindi non si dovrà
definire un n-gram

Il longest common subsequence (MASSIMA sottosequenza comune) consiste nel date due stringhe di lunghezza differente. prendere 
la sottostringa partendo da sinistra a destra piu lunga e comune delle due stringhe, anche se
non nello stesso blocco. Ad esempio:

S=ABAZDC
T=BACBAD

La LCS è ABAD



Invece l algoritmo BLEU  Bilingual Evaluation Understudy Score utilizza una forma modificata di recall e precisione
assume valore da 0 (pessimo) a 1 (perfetto), utilzizato per valutare le traduzioni automatiche



"""




    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
class Summarization():
   

   




  def __init__(self, methods ,pred=False, keywords=False): 
        #pred effettua previsione, validate effettua valutazione 
        
        models_x=['textrank-g','textrank-s','lexrank-s','lsa-s','luhn-s']
        self.models_x=models_x
        
        if type(methods)!= list:
            self.methods=[methods]
        else:
            self.methods=methods
        
        for method in self.methods:
            
            #converte lista in stringa per l'exception
            if method not in self.models_x: 
                stringa=""
                for i in models_x: 
                    stringa=stringa+i+", "
                    
                raise Exception("Il modello "+method+" non  è presente. Puoi scegliere tra "+stringa[:-1])
        
        self.pred=pred
        self.keywords=keywords
        
        
  def getInfo_methods(self):
      print(self.models_x)
        
        
  def summ(self,text,ratio=0.2, split=False, word_count=None, n_sentences=1,methods='textrank-g'):            #funzione che esegue la previsione
    
    
    lang=self.get_lang(text)
    
    parser = PlaintextParser.from_string(text, Tokenizer(lang))

    if (methods=='lexrank-s'):
        
        
        summarizer = LexRankSummarizer()

        summary = summarizer(parser.document, n_sentences ) 
        summary_p=""
        for index in range(len(summary)):
            summary_p=summary_p+str(summary[index])
            
    elif (methods=='textrank-s'):
        
        
        summarizer = TextRankSummarizer()

        summary = summarizer(parser.document, n_sentences) 
        summary_p=""
        for index in range(len(summary)):
            summary_p=summary_p+str(summary[index])
            

            
    elif (methods=='lsa-s'):
        
        
        summarizer = LsaSummarizer()

        summary = summarizer(parser.document, n_sentences) 
        summary_p=""
        for index in range(len(summary)):
            summary_p=summary_p+str(summary[index]) 
            

            
            
    elif (methods=='luhn-s'):
        
        
        summarizer = LuhnSummarizer()

        summary = summarizer(parser.document, n_sentences) 
        summary_p=""
        for index in range(len(summary)):
            summary_p=summary_p+str(summary[index])  
            

            
            
            
    else:          #applica textrank di gensim
        summary_p = gensim_summarize(text,ratio=ratio,word_count=word_count, split=split)

            
            

        
        
    return summary_p


 
            
  def extract_keywords(self,text,ratio=0.3):
            kw = keywords.keywords(text, ratio=ratio).replace("\n"," ").split(" ")
            return kw
                    

  def get_lang(self,testo):

        lang=detect(testo)
        if(lang=='it'):
            lang='italian'
        elif(lang=='en'):
            lang='english'
        else:
            lang=""
            print("lingua non identificata")
            
        return lang
            
        
   
    


  def fit(self,df,field='text',field_summary='summary',ratio=0.2,
          split=False, n_sentences=1, word_count=None, ratio_keyword=0.3):   #
        """
        df=dataframe
        field=variabile testuale
        field_summary=variable dove presente summary
        ratio= percentuale della lunghezza del summary che si desidera
        
        metric= 'rouge-ng' con n=[1,2] e con g=[r,p,f] oppure 'bleu' (da numeri troppo elevati)
        
        method='textrank-g' per textrank con gensim, 'textrank-s' per textrank con summy.
               'lexrank' utilizza lexrank con sumy
    
        """
    
    
    
        """---------------------------[1]PREDIZIONE.--------------------------------------------------"""
        
        
        if self.pred:

            print("********** Predizione **********")
            print("")
            
            

            
            
                
                
            
                
               
                
            for method in self.methods:
            
                  df['summary_'+method]=df[field].apply(
                      self.summ,ratio=ratio,split=split,
                      n_sentences=n_sentences,word_count=word_count, methods=method)
                
                  string_empty=df['summary_'+method].where(df['summary_'+method]=='').dropna().index.values
                  for i in string_empty:
                       print("Row "+str(i)+ " is an empty string in summary_"+method)
            
                  df['summary_'+method]=df['summary_'+method].mask(df['summary_'+method] =="", " ")
                     #sostituisce il record di stringa vuota con stringa con un elemento

                             
                             
            print("Fatto")
            print("")
            
            
            
            
            
            
            

            
        

    
        """---------------------------[3]KEYWORDS.--------------------------------------------------"""

        if self.keywords:
            
            print("********** Keywords **********")
            print("")

            df['keywords'] = df.apply(lambda row: self.extract_keywords( row[field],ratio=ratio_keyword), axis=1)
            
            
            print("Fatto")
            print("")
            

            
            
        return df
    
    
  def evaluate(self,df,field_summary,metrics,type_metric):
    
            """---------------------------[2]VALIDAZIONE.-----------------------------------------------"""
     
    
            print("validazione")
            if type(metrics) != list:
                metrics=[metrics]
             
            list_metric=['rouge-1','rouge-2','rouge-l']
            for metric in metrics:
                if metric not in list_metric:
                    raise Exception ("La metrica "+metric+" non è valida")
                    
            list_typemetric=['f','r','p']

            if type_metric not in list_typemetric:
                    raise Exception ("Il tipo "+type_metric+" non è valido")
            
            rouge = Rouge()
            d={}
            
            df_scores=pd.DataFrame(index=metrics)
            
            for metric in metrics: 
                df_metric=pd.DataFrame()
                for colonna in list(df.columns):
                    if (colonna.find("summary_")!=-1):
                        scores_list=[]


                        for row in df.index:


                            scores = rouge.get_scores(df[colonna][row], df[field_summary][row])
                            scores_list.append(scores)        


                        df_metric[colonna[8:]+"_"+metric]=pd.Series([x[0][metric][type_metric] for x in scores_list])
                        df_scores.loc[metric,colonna] = np.mean(df_metric[colonna[8:]+"_"+metric])
     
                d[metric+'_df']=df_metric
                print("Dataframe per la metrica "+metric+ " salvato")
                    
                        
            def (df,colonna,field_summary,metric):
                                                                                                                                            scores_list=[]


                                                                                                                                                    for row in df.index:


                                                                                                                                                        scores = rouge.get_scores(df[colonna][row], df[field_summary][row])
                                                                                                                                                        scores_list.append(scores)        


                                                                                                                                                    df_metric[colonna[8:]+"_"+metric]=pd.Series([x[0][metric][type_metric] for x in scores_list])
                                                                                                                                                    df_scores.loc[metric,colonna] = np.mean(df_metric[colonna[8:]+"_"+metric])
                
                    
            print("fatto")
            
            return d , df_scores
            
            


            
        
        


    