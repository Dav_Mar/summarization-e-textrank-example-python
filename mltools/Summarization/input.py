try: import pandas as pd 
except: print("there isn't pymongo library")
        
try: import json  
except: print("there isn't json library")

from pprint import pprint
        
try: from pprint import pprint  
except: print("there isn't pprint library")

try: import os  
except: print("there isn't os library")
    
try: import codecs
except: print("there isn't codecs library")
    
try: import numpy as np
except: print("there isn't numpy library")
    

        

class input():
    
    
    """ This class read files from diffrent sources. In this moment its can:
            1. Read data from .csv
            2. Read a json file
            3. Read from a folder with many file .txt, when each file has a text file, this text
               and will write this as a row of a panda's dataframe.

    Args:
    -----
        CSV     (boolean)   >>> Set it to True if you want to read data on .csv file. 
                                    Default is equal to False.
        MYSQL   (boolean)   >>> Set it to True if you want to read data on a mysql. 
                                    Default is equal to False.
        JSON    (boolean)   >>> Set it to True if you want to read data on a json. 
                                    Default is equal to False.
        MONGODB (boolean)   >>> Set it to True if you want to read data on a json. 
                                    Default is equal to False.
        FOLD_TO_DF (boolean)>>> Set it to True if you want to read data text in a folder. 
                                    Default is equal to False.
        optimize (boolean)  >>> Set it to True if you want to reduce the memory consume of the dataframe. 
                                    Default is equal to False.
    """

    
    def __init__(self,CSV=False, MYSQL=False, JSON=False, MONGODB=False, FOLD_TO_DF=False, optimize=False):
        self.CSV=CSV
        self.MYSQL=MYSQL
        self.JSON=JSON
        self.MONGODB=MONGODB
        self.FOLD_TO_DF=FOLD_TO_DF
        self.optimize=optimize
        
    def read_csv(self, file_name,sep,header):
        
        data=pd.read_csv(file_name,sep=sep,header=None)
        return data
    
    
    def read_json(self,file_name):
        with open(file_name) as f:
            data = json.load(f)
        return data
    
    

    
    
    
    
    def folders_to_df(self,path):
       """
    
       Questa funzione, data una cartella contenente a sua volta delle cartelle dove all'interno di
       ciascuna ci siano files .txt (penso funzioni anche con altre estensione tipo csv), in cui su ogni
       file c'è un testo. Ad esempio questa path = "/home/nome/Desktop/data/News Articles/" dove si ha
       "/home/nome/Desktop/data/News Articles/politics/" e "/home/david/Desktop/data/News Articles/sport/",
       quindi una cartella New Articles con all'interno due altre cartelle, politics e sport. All'interno di
       quest'ultime, abbiamo ad esempio "/home/nome/Desktop/data/News Articles/politics/001.txt" e altri files
       di questo tipo...
    
       Questa funziona, presa path = "/home/david/Desktop/data/News Articles/politics/", restituirà
       un dataframe dove per ogni riga viene inserito il contenuto di ogni file .txt
    
    
       """
    
    
       #-----------------------------------------funzione interna-------------------------------
    
       def create(new_path,files):


            """Prende una path, ad esempio path = "/home/david/Desktop/data/News Articles/politics/", dove sono presenti
            i files da leggere. I files vengono dati come lista, ad esempio 
            ['001.txt','002.txt','003.txt','004.txt','005.txt',...,'410.txt']. Restituisce un dataset dove a ogni
            riga viene inserito il contenuto di ciascun file
            """
    
            dataset = pd.DataFrame()      
            n=0                             #contatore
            for file in files:              #legge ogni file all'interno della lista
                new_path2=new_path+file
                t=pd.read_csv(new_path2, sep='/n',header=None,engine='python')       
                #apre il file di testo
            
                                             #se c'è solo una riga nel file
                if(len(t)==0):
                    text=str(t).replace("Empty DataFrame\nColumns: [","").replace("]\nIndex: []","")
                else:                           #nel caso di piu righe
                    text=str(t.iloc[[0]].values)[3:-3]
                    for i in range(1,len(t)):
                        text=text+" "+str(t.iloc[[i]].values)[3:-3]
                        text=text.replace("'","°").replace("\\","")
                dataset=dataset.append(pd.Series(text, name=str(n)))            #scrive sul dataset il testo
                n+=1
            return dataset
    
            #-------------------------------fine funzione interne---------------------------------------    
    
    
    
       dataset_f = pd.DataFrame()      
       folder_list = os.listdir(path)          #legge le cartelle nella path
       for folder in folder_list:             
          
          new_path=path+"/"+folder+"/"
          n_text = os.listdir(new_path)       #per ogni cartella, legge i files presenti
        
          dataset=create(new_path,n_text)     #crea con la funzione, la riga del dataset

          dataset_f=dataset_f.append(dataset) #che viene inserita
        
        
        
       dataset_f = dataset_f.reset_index(drop=True)   #reset dell'index
    
       return dataset_f    
    
    
    
    
    
    
    def riduci(self,df):
     """funzione che prende un dataframe e ne converte i tipi di variabili per consumare meno
     memoria"""
     start_mem = df.memory_usage().sum() / 1024**2
     print('Memoria utilzzata dal dataframe originale {:.2f} MB'.format(start_mem))
    
     for col in df.columns:
         col_type = df[col].dtype
        
         if col_type != object:
             c_min = df[col].min()
             c_max = df[col].max()
             if str(col_type)[:3] == 'int':
                 if c_min > np.iinfo(np.int8).min and c_max < np.iinfo(np.int8).max:
                     df[col] = df[col].astype(np.int8)
                 elif c_min > np.iinfo(np.int16).min and c_max < np.iinfo(np.int16).max:
                     df[col] = df[col].astype(np.int16)
                 elif c_min > np.iinfo(np.int32).min and c_max < np.iinfo(np.int32).max:
                     df[col] = df[col].astype(np.int32)
                 elif c_min > np.iinfo(np.int64).min and c_max < np.iinfo(np.int64).max:
                     df[col] = df[col].astype(np.int64)  
             else:
                 if c_min > np.finfo(np.float16).min and c_max < np.finfo(np.float16).max:
                     df[col] = df[col].astype(np.float16)
                 elif c_min > np.finfo(np.float32).min and c_max < np.finfo(np.float32).max:
                     df[col] = df[col].astype(np.float32)
                 else:
                     df[col] = df[col].astype(np.float64)
         else:
             df[col] = df[col].astype('category')

     end_mem = df.memory_usage().sum() / 1024**2
     print('Memoria del dataframe ottimizzato: {:.2f} MB'.format(end_mem))
     print('Diminuito del {:.1f}%'.format(100 * (start_mem - end_mem) / start_mem))
    
     return df
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
        
        
        
    def read(self, file_name,sep=',',header=None):
        df=pd.DataFrame()
        
        if self.FOLD_TO_DF:
            print("Sto leggendo files in  "+file_name)
            try:
                df=self.folders_to_df(file_name)
            except:
                print("Impossibile leggere files in " + file_name)
                
        elif self.CSV:
            print("Read csv file")
            try:
                df=self.read_csv(file_name,sep=sep,header=header)
                
            except:
                print("Impossibile leggere files in " + file_name)
                
        elif self.MYSQL:
            print("")
           #something
        
        elif self.JSON:
            print("Read json file")
            try:
                df=self.read_json(file_name) 
            except:
                print("Impossibile leggere files in " + file_name)
            
        elif self.MONGODB:
            print("")
            #something
            
        
        else:
            print("Nessun commando valido")
            
            
        if self.optimize:
            print("ottimizzazione del dataframe")
            df=self.riduci(df)
            
        print("finish")
    
        return df
            
