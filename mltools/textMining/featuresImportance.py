import numpy as np
import matplotlib.pyplot as plt
from nltk import FreqDist


def get_most_important_features(vectorizer, model, n=5):
    index_to_word = {v:k for k,v in vectorizer.vocabulary_.items()}
    
    # loop for each class
    classes ={}
    for class_index in range(model.coef_.shape[0]):
        word_importances = [(el, index_to_word[i]) for i,el in enumerate(model.coef_[class_index])]
        sorted_coeff = sorted(word_importances, key = lambda x : x[0], reverse=True)
        tops = sorted(sorted_coeff[:n], key = lambda x : x[0])
        bottom = sorted_coeff[-n:]
        classes[model.classes_[class_index]] = {
            'tops':tops,
            'bottom':bottom
        }
    return classes

def plot_important_words(importance, class_name, dpi = 200, figsize = (6, 3)):

    top_scores = [a[0] for a in importance[class_name]['tops']]
    top_words = [a[1] for a in importance[class_name]['tops']]

    y_pos = np.arange(len(top_words))
    top_pairs = [(a,b) for a,b in zip(top_words, top_scores)]
    top_pairs = sorted(top_pairs, key=lambda x: x[1])

    top_words = [a[0] for a in top_pairs]
    top_scores = [a[1] for a in top_pairs]
    
    fig = plt.figure(figsize= figsize, dpi = dpi)  
    plt.barh(y_pos,top_scores, align='center', alpha=1)
    plt.title(class_name, fontsize=9)
    plt.yticks(y_pos, top_words, fontsize=7)
    plt.xticks(fontsize=7)
    plt.xlabel('Importance', fontsize=7)
    plt.grid(lw=0.4)
    plt.xlim(0, np.max(top_scores) + 0.5)
    plt.show()

def plot_word_freq(df, target, col, ticks_size=11, label_size=14,
                   title_size=16, x_label='', y_label=''):
    for label in np.unique(df[target]):    
        words_list = []
        
        for line in df[df[target] == label][col]:
            for word in line:
                words_list.append(word)
        
        freqdist = FreqDist(words_list)
        plt.figure(figsize=(16,4))
        plt.title(label, fontsize=title_size)
        plt.xticks(size=ticks_size)
        plt.yticks(size=ticks_size)
        plt.xlabel(x_label, size=label_size)
        plt.ylabel(y_label, size=label_size)

        freqdist.plot(50)
